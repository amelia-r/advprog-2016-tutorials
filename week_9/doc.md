Web Chat App

This is a simple chat app that allows different browsers to send messages to each other. In this version however, the app can not recognize/ identify the other browser. It can receive messages sent by the other browser but cannot identify it.

To use this app, look into the "example" folder. Then from that folder, open the "templates" folder, there you will see the index.html file, which is the main interface you will be using to send messages.

Before you open the html file, you must first activate the SocketIO. To do that, open your terminal and cd to the "example" folder. For safety measures, activate the virtual environment in that folder using venv\bin\activate. After you've done that, type in python week9.py and press enter. The SocketIO would then start running and this is when you can open index.html.

In the address bar, type in localhost:5000 and press enter, you will be directed to the interface page of the Web Chat App and you can start sending messages.